import random
from django.core.management.base import BaseCommand
from django_seed import Seed

from test.models import Person, Vehicle

class Command(BaseCommand):
    help = "Seed the database with fake users"

    def handle(self, *args, **kwargs):
        Person.objects.all().delete()
        Vehicle.objects.all().delete()

        return

        seeder = Seed.seeder(locale="en_CA")

        seeder.add_entity(Person, 10000)
        seeder.add_entity(Vehicle, 30)

        members = seeder.execute()
