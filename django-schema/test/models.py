from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=50)
    age = models.IntegerField()


class Vehicle(models.Model):
    name = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    year = models.IntegerField()
    owner = models.ForeignKey(Person, on_delete=models.CASCADE)
