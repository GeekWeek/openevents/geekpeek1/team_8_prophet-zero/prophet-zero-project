# Django Schema

This is a Django project for creating and managing the project's database
schema. For it, you'll need Poetry installed, and a Python >= 3.9.

First, make sure you have the database from the `docker-compose.yml` file in the
directory above. To install the project:

```bash
poetry install
poetry run manage.py migrate
```

Then, you can load the sample database:

```bash
poetry run manage.py loaddata db.json
```
