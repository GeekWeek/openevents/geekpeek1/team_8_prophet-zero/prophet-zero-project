from django.db import models

from dynamic_behaviours.models import DynamicBehaviour
from static_behaviours.models import StaticBehaviour


class Malware(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True, default="")

    static_behaviours = models.ForeignKey(
        StaticBehaviour, on_delete=models.CASCADE, null=True, blank=True
    )

    dynamic_behaviours = models.ForeignKey(
        DynamicBehaviour, on_delete=models.CASCADE, null=True, blank=True
    )

    def __str__(self):
        return f"{self.name} ({self.language}, {self.compilation_date})"
