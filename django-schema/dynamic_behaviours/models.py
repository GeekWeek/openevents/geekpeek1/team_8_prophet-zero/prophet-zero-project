from django.db import models
from django.contrib.postgres.fields import ArrayField

## Primatives
class DynamicFileName(models.Model):
    name = models.TextField(unique=True)


class Syscall(models.Model):
    name = models.TextField(unique=True)


class IP(models.Model):
    ip = models.TextField(unique=True)

class DynamicLibraries(models.Model):
    name = models.TextField(unique=True)


## Composite
class FileIO(models.Model):
    syscall = models.ForeignKey(Syscall, on_delete=models.CASCADE)
    file_name = models.ForeignKey(DynamicFileName, on_delete=models.CASCADE)


class NetworkIO(models.Model):
    from_ip = models.ForeignKey(IP, on_delete=models.CASCADE, related_name="from_ip")
    to_ip = models.ForeignKey(IP, on_delete=models.CASCADE, related_name="to_ip")
    data = models.TextField()


## Dynamic Behaviours
class DynamicBehaviour(models.Model):
    parent = models.TextField(default="")
    pid = models.IntegerField(null=True, blank=True)
    num_threads = models.IntegerField(default=1)
    children = models.TextField(default="")

    # Relates to Syscall
    syscalls_list = ArrayField(models.IntegerField(), default=list)
    # Relates to FileIO
    file_io_list = ArrayField(models.IntegerField(), default=list)
    # Relates to Libraries
    libraries = ArrayField(models.IntegerField(), default=list)