from django.db import models
from django.contrib.postgres.fields import ArrayField

## Primatives
class Import(models.Model):
    name = models.CharField(max_length=128, default="", unique=True)

    def __str__(self):
        return self.name


class Library(models.Model):
    name = models.CharField(max_length=128, default="", unique=True)

    def __str__(self):
        return self.name


class StringValue(models.Model):
    value = models.TextField(unique=True)


class DataSection(models.Model):
    name = models.TextField(unique=True)


class IoCType(models.Model):
    name = models.TextField(unique=True)


class IoCValue(models.Model):
    value = models.TextField(unique=True)


class Command(models.Model):
    name = models.TextField(unique=True)


class StaticFileName(models.Model):
    name = models.TextField(unique=True)


## Composite
class ImportLibPair(models.Model):
    import_name = models.ForeignKey(Import, on_delete=models.CASCADE)
    library_name = models.ForeignKey(Library, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.import_name} -> {self.library_name}"


class IoC(models.Model):
    ioctype = models.ForeignKey(IoCType, on_delete=models.CASCADE)
    iocvalue = models.ForeignKey(IoCValue, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.ioctype} -> {self.iocvalue}"


class Strings(models.Model):
    value = models.ForeignKey(StringValue, on_delete=models.CASCADE)
    section = models.ForeignKey(DataSection, on_delete=models.CASCADE)

    text_type = models.CharField(max_length=6, default="ascii")


## Static Behaviours
class StaticBehaviour(models.Model):
    arch = models.CharField(max_length=5, null=True, blank=True, default="")
    bin_type = models.CharField(max_length=5, null=True, blank=True, default="")
    class_type = models.CharField(max_length=5, null=True, blank=True, default="")
    binary_size = models.BigIntegerField(default=0)
    compilation_date = models.DateTimeField(null=True, blank=True)
    operating_system = models.CharField(
        max_length=10, null=True, blank=True, default=""
    )
    language = models.CharField(max_length=10, null=True, blank=True, default="")

    import_pairs = models.ManyToManyField(ImportLibPair, blank=True)

    strings = models.ManyToManyField(Strings, blank=True)
    indicators_of_compromise = models.ManyToManyField(IoC, blank=True)

    files = models.ManyToManyField(StaticFileName, blank=True)
    commands = models.ManyToManyField(Command, blank=True)
