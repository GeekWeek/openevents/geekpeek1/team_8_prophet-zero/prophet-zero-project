from django.apps import AppConfig


class StaticBehavioursConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'static_behaviours'
