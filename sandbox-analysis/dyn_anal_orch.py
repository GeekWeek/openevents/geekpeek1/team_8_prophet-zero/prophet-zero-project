import os
from re import L
import dynamic_analysis as da

target = "sample_files"
target_dirs = os.listdir(target)

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cluster_like_samples as cluster
from sklearn.decomposition import PCA
features =  []
all_libs_lists = []
all_syscalls = []

def test_pca(data, labels, n_clusters=16):
    pca = PCA(2)
    pca.fit(data)
    co1 = pca.transform(data)

    for i in range(n_clusters):
        coi=co1[labels==i]
        plt.scatter(coi[:,1], coi[:,0])
    plt.show()

def parse_syscalls(calls, uniq):
    call_counts = np.zeros((len(calls), uniq.shape[0]))
    i = 0
    for trace in calls:
        for c in trace:
            idx = np.where(uniq==c)[0][0]
            call_counts[i, idx]+=1
        i+=1
    return call_counts

from sklearn.preprocessing import LabelEncoder
def df_to_numpy(df):
    data = np.zeros((len(df), len(df.columns)))
    for i in range(df.shape[1]):
        enc = LabelEncoder()
        col = df.iloc[:, i]
        xi = enc.fit_transform(col)
        data[:, i]=xi
    return data

errors = []
targets = []
for sample in target_dirs:
    target_dir = os.path.join(target, sample)

    target_name_file = os.path.join(target_dir, "id.txt")
    f = open(target_name_file, "r")
    tname = f.read().split("\n")[0]
    target_name = tname.replace(".", "_")
    try:
        pe_header, libs_list, syscalls, threads, network_traffic = da.analyse(target_dir, target_name, os.path.join("zoo_samples", tname))
        targets.append(target_name)
        pe_header["network"]=network_traffic
        pe_header["threads"]=threads
        features.append(pe_header)
        all_syscalls.append(syscalls)
        all_libs_lists.append(list(set(libs_list)))
    except Exception as e:
        print(e)
        errors.append(e)
df = pd.DataFrame(features)
all_calls = []
for a in all_syscalls:
    for x in a:
        all_calls.append(x)
uniq = np.unique(np.array(all_calls).flatten())
s1 =parse_syscalls(all_syscalls, uniq)

all_libs = []
for l in all_libs_lists:
    for li in l:
        all_libs.append(li)
uniq = np.unique(np.array(all_libs).flatten())
s2 = parse_syscalls(all_libs_lists, uniq)

data = df_to_numpy(df)

X = np.hstack((data, s1, s2))

import cnn.type_class as tclf
cluster_labels = cluster.cluster_samples(X)
test_pca(X, cluster_labels)

""""
labels, probs = tclf.run(all_syscalls)

import json

for i in range(len(targets)):
    t = targets[i]
    out_file = os.path.join("out", t+"_ml_data.json")
    ml_data = {
        "cluster": str(cluster_labels[i]),
        "class_label": labels[i],
        "confidences": probs[i]
    }
    with open(out_file, "w") as f:
        json.dump(ml_data, f, indent=4)
"""

print("Done...")
for err in errors:
    print(err)
