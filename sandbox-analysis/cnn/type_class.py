
import numpy as np
from tensorflow.keras.layers import Dense,Input,LSTM
from tensorflow.keras.models import Sequential
from keras.preprocessing import sequence
from tensorflow.keras.layers import Conv1D, MaxPooling1D, Embedding, Dropout,Flatten
from sklearn.preprocessing import LabelEncoder, LabelBinarizer

from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint, Callback
from tensorflow.python.keras.layers import embeddings

class BinClassifier:
    def __init__(self, input_dim, words_dim, embedding_dim, out_dim):
        self.input_dim = input_dim
        self.words_dim = words_dim
        self.embedding_dim = embedding_dim
        self.out_dim = out_dim
    
    def build(self):
        self.model = Sequential()
        self.model.add(Embedding(self.words_dim, self.embedding_dim, input_length=self.input_dim))
        self.model.add(Conv1D(filters=16, kernel_size=2, padding='same', activation='relu'))
        self.model.add(Dropout(0.5))

        self.model.add(Conv1D(filters=8, kernel_size=2, padding='same', activation='relu'))
        self.model.add(MaxPooling1D(pool_size=1))

        self.model.add(Flatten())
        self.model.add(Dense(64, activation='relu'))
        self.model.add(Dense(32, activation='relu'))
        #model.add(Dropout(0.5))
        self.model.add(Dense(self.out_dim, activation='softmax'))
        self.model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    def train(self, X_train, y_train):
        self.build()
        mc = ModelCheckpoint('best_model.h5', monitor='loss', mode='min', verbose=0, save_best_only=True)
        self.model.fit(X_train, y_train, shuffle=True, epochs=400, batch_size=64, verbose=1, callbacks=[mc])

        self.saved_model = load_model('best_model.h5')
    
    def load_model(self):
        self.saved_model = load_model('best_model.h5')

    def predict(self, X):
        pred = self.saved_model.predict(X)
        return np.argmax(pred, axis=-1), pred

from keras.preprocessing import text, sequence
from keras.preprocessing.text import Tokenizer
from sklearn.model_selection import train_test_split

mlen = 120
def lines_to_sequences(lines):
    calls = []
    max_len = 0
    mlen = 120
    for line in lines:
        if type(line) is str:
            cl = line.split(" ")
        else:
            cl = line
        #calls.append(np.array(cl[:100]+cl[-100:]))
        seq = []
        for s in cl:
            if s[:2]=="nt":
                seq.append(s)
        if len(seq) > mlen:
            seq = seq[:60]+seq[-60:]
        if len(seq) < mlen:
            r = mlen - len(seq)
            pad = ["0"]*r
            seq = seq + pad
    
        calls.append(seq)
        if len(calls[-1]) > max_len:
            max_len = len(calls[-1])
    
    return calls

def preprocess(lines, sym):
    new_lines = []
    for line in lines:
        seq = []
        for s in line:
            s = s.lower()
            if s in sym:
                seq.append(s)
        new_lines.append(seq)
    return new_lines
        
import os
def run(new_data):
    f = open(os.path.join("cnn", "all_analysis_data.txt"), "r")
    lines = f.read().split("\n")

    f = open(os.path.join("cnn", "labels.csv"), "r")
    y_lines = f.read().split("\n")

    yenc = LabelEncoder()
    y_enc = yenc.fit_transform(y_lines)
    print(y_enc[:20])

    no_classes = np.unique(y_enc).shape[0]

    calls = lines_to_sequences(lines)

    new_lines = preprocess(new_data, np.unique(calls).tolist())
    new_calls = lines_to_sequences(new_lines)

    # maximum length of sequence, everything afterwards is discarded!
    max_length = mlen
    #create and fit tokenizer
    tokenizer = Tokenizer(char_level=True)
    tokenizer.fit_on_texts(calls)
    #represent input data as word rank number sequences
    X = tokenizer.texts_to_sequences(calls)
    X_test = tokenizer.texts_to_sequences(new_calls)
    X = sequence.pad_sequences(X, maxlen=max_length)
    X_test = sequence.pad_sequences(X, maxlen=max_length)

    from sklearn.model_selection import train_test_split

    #X_train, X_test, y_train, y_test = train_test_split(X, y_enc, test_size=0.1)
    X_train, y_train = X, y_enc

    print(X_train.shape[0])


    embedding_dim = 8
    words_dim = len(tokenizer.word_index)+1
    from sklearn.metrics import classification_report

    clf = BinClassifier(max_length, words_dim, embedding_dim, no_classes)
    clf.train(X_train, y_train)

    labels, pred = clf.predict(X_test)
    pred_list =  pred.tolist()
    p = []
    for plist in pred_list:
        pi = [str(x) for x in plist]
        p.append(pi)
    
    return yenc.inverse_transform(labels), p

#pred = clf.predict(X_test)
#print(classification_report(y_test, pred))
