import os
import r2pipe
import json
import re
import base64
import pefile
import iocextract
# iocstrings samples/Win32.GravityRAT.exe
"""
    .text → the actual code the binary runs
    .data → read/write data (globals)
    .rdata → read-only data (strings)
    .bss → Block Storage Segment (uninitialzed data format), often merged with the .data section
    .idata → import address table, often merged with .text or .rdata sections
    .edata → export address table
    .pdata → some architectures like ARM, MIPS use these sections structures to aid in stack-walking at run-time
    PAGE* → code/data which it's fine to page out to disk if you're running out of memory
    .reolc → relocation information for where to modify the hardcoded addresses
    .rsrc → resources like icons, other embedded binaries, this section has a structure organizing it like a filesystem
"""
import time

from subprocess import Popen, PIPE
import asyncio

async def call_browser(args):
    cmd = " ".join(args)
    process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
    # Wait for output:
    stdout, stderr = await process.communicate()
    print(stderr.decode("utf-8"))
    return stdout

def call_external_script(file_name, args):
    args =[file_name]+args
    process = Popen(args, stdout=PIPE, stderr=PIPE)
    time.sleep(4)
    stdout, stderr = process.communicate()
    print(stderr.decode("utf-8"))
    return stdout

def extract_iocs(content):
    out_data = list(iocextract.extract_iocs(content, refang=True))
    print(out_data)
    return out_data


def get_pe_header(filename):
    pe = pefile.PE(filename)
    print(pe.header)
    print(pe.print_info())
    # "emagic", "ecblp", "ecp","ecrlc","ecparhdr", "eminalloc","emaxalloc","ess","esp", "ecsum","eip","ecs","elfarlc","eovno","eres", "eoemid","eoeminfo","eres2","e_lfanew"

    # "Machine","NumberOfSections","CreationYear","PointerToSymbolTable", "NumberOfSymbols","SizeOfOptionalHeader","Characteristics"
    
    # Check if it is a 32-bit or 64-bit binary
    if hex(pe.FILE_HEADER.Machine) == '0x14c':
        print("This is a 32-bit binary")
    else:
        print("This is a 64-bit binary")

    headers = {
    "e_magic": pe.DOS_HEADER.e_magic,
    "e_cblp": pe.DOS_HEADER.e_cblp,
    "e_cp": pe.DOS_HEADER.e_cp,
    "e_crlc": pe.DOS_HEADER.e_crlc,
    "e_cparhdr": pe.DOS_HEADER.e_cparhdr,
    "e_minalloc": pe.DOS_HEADER.e_minalloc,
    "e_maxalloc": pe.DOS_HEADER.e_maxalloc,
    "e_ss": pe.DOS_HEADER.e_ss,
    "e_sp": pe.DOS_HEADER.e_sp,
    "e_csum": pe.DOS_HEADER.e_csum,
    "e_ip": pe.DOS_HEADER.e_ip,
    "e_cs":pe.DOS_HEADER.e_cs,
    "e_lfarlc": pe.DOS_HEADER.e_lfarlc,
    "e_ovno": pe.DOS_HEADER.e_ovno,
    "e_res":pe.DOS_HEADER.e_res,
    "e_oemid": pe.DOS_HEADER.e_oemid,
    "e_oeminfo": pe.DOS_HEADER.e_oeminfo,
    "e_res2": pe.DOS_HEADER.e_res2,
    "e_lfanew": pe.DOS_HEADER.e_lfanew,
    #"Machine": pe.FILE_HEADER.Machine,
    "NumberOfSections": pe.FILE_HEADER.NumberOfSections,
    #"CreationYear": pe.FILE_HEADER.dump_dict()['TimeDateStamp']['Value'].split('[')[1][:-1].split(" ")[-2],
    #"Characteristics": pe.FILE_HEADER.Characteristics,
    "NumberOfSymbols": pe.FILE_HEADER.NumberOfSections,
    #"PointerToSymbolTable": pe.FILE_HEADER.PointerToSymbolTable,
    #"SizeOfOptionalHeader": "",
    }

    #file_signature = pe.NT_HEADERS.Signature
    return headers

import hashlib
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

base64_matcher = re.compile("(?:[A-Za-z0-9\+\/=]{4})*")
registry_keys_list = [
    "HKCU", "HKLM", "HKEY_LOCAL_MACHINE", "HKEY_CURRENT_USER", "HKU", "HKEY_USERS", "HKEY_CLASSES_ROOT", "HKCR", "SOFTWARE", "SYSTEM", "SECURITY", "SAM", "HARDWARE", "DRIVERS"
]
extensions_list = [".exe", ".bat", ".php", ".vbs", ".html", ".js", ".dll", ".bin", ".dat", ".sh", ".java", ".ps1", ".jar"]

f0 = open("win_procs.txt", "r")
tools = [x for x in f0.read().split("\n")]

def get_file_info(name):
    hash = md5(name)
    r2 = r2pipe.open(name, flags=['-2', '-e', 'bin.verbose=false'])
    r2.cmd("aaaa")
    
    files = []
    cmds = []

    file_info = r2.cmd("i")
    iocs = []
    ie = r2.cmd("iaj")
    info = json.loads(ie)
    info_obj = info["info"]
    file_info = {
        "arch": info_obj["arch"],
        "bintype": info_obj["bintype"],
        "class": info_obj["class"],
        "compiled": info_obj["compiled"],
        "os": info_obj["os"],
        "lang": info_obj["lang"],
        "hash": hash, 
        "filesize": os.stat(name).st_size
    }
    """
    funcs = r2.cmd("aflj")
    print(funcs)

    bb = r2.cmd("afb")
    print(bb)
    """

    imports =  info["imports"]
    imports_obj = []
    libs_list = []
    lib_calls_list = []
    for obj in imports:
        aname = obj["name"]
        libname = obj["libname"]

        imports_obj.append({"name": aname, "lib_name": libname})
        libs_list.append(libname)
        lib_calls_list.append(aname)

    if "CRYPT32.dll" in libs_list: 
        iocs.append({"indicator_type": "technique", "value": "crypto"})
    
    if "socket" in lib_calls_list or "recv" in lib_calls_list or "connect" in lib_calls_list or "Socket" in lib_calls_list:
        iocs.append({"indicator_type": "technique", "value": "net"})
    

    #print("References:\n-----")
    #lb = r2.cmd("ilj")
    

    strings = r2.cmd("izzj")
    #print(strings)
    strings = json.loads(strings)
    strings_objs = []
    strings_string = ""
    for s in strings:
        section = s["section"]
        stype = s["type"]
        svalue = s["string"]
        #print("Section: "+section)
        #print("Type: "+stype)
        #print("Value: "+svalue)
        
        result = {"section": section, "type": stype, "string": svalue}

        if re.match(base64_matcher, svalue):
            try:
                result["decoded"] = base64.standard_b64decode(svalue).decode()
            except:
                pass
            
        if re.match(r'[C|c]:\\', svalue):
            iocs.append({"indicator_type": "file path", "value": svalue})
        
        if "select * from" in svalue.lower():
            iocs.append({"indicator_type": "wmi query", "value": svalue})

        if "\\" in svalue:
            for i in registry_keys_list:
                if i.lower() in svalue.lower():
                    iocs.append({"indicator_type": "registry access", "value": svalue})
        
        if "."+svalue.split(".")[-1].lower() in extensions_list:
            files.append(svalue)
        
        if len(svalue) > 3 and svalue.split(" ")[0].lower() in tools:
            cmds.append(svalue)

        if not (svalue in libs_list or svalue in lib_calls_list or (stype=="ascii" and len(svalue) < 8 and ("\\" in svalue))):
            strings_objs.append(result)
            strings_string  = strings_string + "\n" + result["string"]


    if "isVM" in strings_string or "detectVM" in strings_string or "VirtualBox" in strings_string or "DetectVM" in strings_string or "Virtual Machine" in strings_string:
        iocs.append({"indicator_type": "technique", "value": "detect vm"})

    if "UPX" in strings_string:
        iocs.append({"indicator_type": "technique", "value": "UPX - packing"})

    if "Code Signing CA" in strings_string:
        iocs.append({"indicator_type": "technique", "value": "Code Signing"})

    #with open("tmp_strings.txt", "w") as f:
    #    f.write(strings_string)
    
    pe_headers = get_pe_header(name)
    #fn = "iocextract"
    #result = asyncio.run(call_browser([fn, "--input", "tmp_strings.txt", "--strip-urls","--extract-emails", "--extract-ips", "--extract-ipv4s", "--extract-ipv6s", "--extract-urls", "--extract-hashes"]))
    result = extract_iocs(strings_string)
    print(result)
    
    for item in result:
        if "://" in item:
            iocs.append({"indicator_type": "url", "value": item})
        elif re.match(r'\d+:\d+:\d+', item):
            iocs.append({"indicator_type": "ipv6", "value": item})
        elif re.match(r'\d+.\d+.\d+', item):
            if not item[:3]=="0.":
                iocs.append({"indicator_type": "ipv4", "value": item})
        elif "@" in item:
            iocs.append({"indicator_type": "email", "value": item})
        elif ":::" in item or re.match(r'\d+:::', item):
            continue
        else:
            iocs.append({"indicator_type": "hash", "value": item})
        
    #result = result.decode("utf-8")
    #print(result)
    file_info["libraries"] = imports_obj
    file_info["strings"]=strings_objs
    file_info["iocs"] = iocs
    file_info["files"] = list(set(files))
    file_info["cmds"] = list(set(cmds))

    #return {"info": file_info, "libraries": imports_obj, "strings": strings_objs, "iocs": iocs, "files": list(set(files)), "cmds": list(set(cmds))}, pe_headers, libs_list
    return file_info, pe_headers, libs_list

"""
#test_sample = os.path.join("samples","malware.exe")
test_sample = "/home/crazyeights/Desktop/prophet-zero-project/sandbox-analysis/samples/Win32.GravityRAT.exe"
#get_pe_header(test_sample)

r = get_file_info(test_sample)
rstr = json.dumps(r, indent=2)
with open("out1.txt", "w") as f:
    f.write(rstr)
"""
