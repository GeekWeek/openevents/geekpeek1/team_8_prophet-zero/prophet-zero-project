import sys
import os
import gzip
import csv
import json

def parse_sc_dat(in_file, out_file, target_asid=[]):
    calls_file = "win32k.csv"
    f1 = open(calls_file)
    csvreader = csv.reader(f1)
    next(csvreader)
    calls_lookup = {}

    for row in csvreader:
        call_name = row[0]
        call_hex = row[19]
        calls_lookup[call_hex] = call_name

    nt_calls_file = "nt.csv"
    f2= open(nt_calls_file)
    csvreader = csv.reader(f2)
    next(csvreader)
    #calls_lookup = {}

    for row in csvreader:
        call_name = row[0]
        call_hex = row[26]
        calls_lookup[call_hex] = call_name


    f = gzip.open(in_file, "rb")
    n = os.fstat(f.fileno()).st_size
    calls = []
    call_list = []
    i =0
    while i < n:
        asid = str(hex(int.from_bytes(f.read(4), byteorder="little")))
        sysid = int.from_bytes(f.read(2), byteorder="little")
        no_args = int.from_bytes(f.read(2), byteorder="little")
        """
        print(asid)
        print(sysid)
        print(no_args)
        """

        #print(sysid)

        #sc_code = hex(sysid)
        sc_code = "0x%0.4x" % sysid
        #print(sc_code)
        if sc_code in calls_lookup:
            syscall = calls_lookup[sc_code]
        else:
            syscall = "error"
    
        i+=8
        args = []
        if no_args > 0:
            for a in range(no_args):
                arg = int.from_bytes(f.read(8), byteorder="little")
                args.append(arg)
                i+=8
    
        if len(target_asid) > 0 and asid in target_asid:
            calls.append({"asid": asid, "call": syscall, "sysid": sc_code, "no_args": no_args, "args": args})
            call_list.append(syscall)

    cstr = json.dumps(calls, indent=2)
    with open(out_file, "w") as f:
        f.write(cstr)
    return call_list