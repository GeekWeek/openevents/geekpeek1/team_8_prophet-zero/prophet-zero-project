import os
import sys
import time
import subprocess
import argparse
import string
import shutil

# Configs
PANDA_BASE = os.path.join(os.path.sep, "home", "crazyeights", "panda")
IMG_BASE = os.path.join(os.path.sep, "home", "crazyeights", "Desktop", "sandbox")
PANDA_x86 = os.path.join(PANDA_BASE, "build", "i386-softmmu", "panda-system-i386")
qcow_file = os.path.join(IMG_BASE, "IE9_win7_disk1.qcow2")


PANDA_flags = [
        "-monitor", "stdio",
        "-show-cursor",
        "-m", "8192",
        "-loadvm", "3",
        qcow_file
    ]
TIME_TO_EXECUTE = 60

def log_info(msg):
    print("[+] %s"%(msg))

def log_exit(msg):
    print("[-] %s"%(msg))
    exit(0)

def guest_type(s, p):

    keymap = {
        '-': 'minus',
        '=': 'equal',
        '[': 'bracket_left',
        ']': 'bracket_right',
        ';': 'semicolon',
        '\'': 'apostrophe',
        '\\': 'backslash',
        ',': 'comma',
        '.': 'dot',
        '/': 'slash',
        '*': 'asterisk',
        ' ': 'spc',
        '_': 'shift-minus',
        '+': 'shift-equal',
        '{': 'shift-bracket_left',
        '}': 'shift-bracket_right',
        ':': 'shift-semicolon',
        '"': 'shift-apostrophe',
        '|': 'shift-backslash',
        '<': 'shift-comma',
        '>': 'shift-dot',
        '?': 'shift-slash',
        '\n': 'ret',
    }

    for c in s:
        if c in string.ascii_uppercase:
            key = 'shift-' + c.lower()
        else:
            key = keymap.get(c, c)

        os.write(p.stdin.fileno(), ("sendkey %s\n"%(key)).encode("utf-8"))
        
        time.sleep(.5)

def record_execution(sample, recording_time):
    '''
    
    '''
    log_info("Recording execution %s"%(sample))
    log_info("Recording for %d seconds"%(recording_time))


    # Create new temporary sample
    new_sample = "sample"
    shutil.copy(sample, new_sample)


    # Create an ISO file of the sample file
    cmd = []
    cmd.append("/usr/bin/genisoimage")
    cmd.append("-iso-level")
    cmd.append("4")
    cmd.append("-l")
    cmd.append("-R")
    cmd.append("-J")
    cmd.append("-o")
    cmd.append("sample.iso")
    cmd.append(new_sample)    

    try:
        subprocess.check_call(cmd)
        log_info("Made an iso file for the sample")
    except Exception:
        print(traceback.format_exc())
        print(sys.exc_info()[0])
        log_exit("Could not make any iso file for the sample")

    # Launch PANDA
    cmd = []
    cmd.append(PANDA_x86)
    for flag in PANDA_flags:
        cmd.append(flag)

    panda_stdout_path = "panda.stdout"
    panda_stderr_path = "panda.stderr"
    panda_stdout = open(panda_stdout_path, 'w+')
    panda_stderr = open(panda_stderr_path, 'w+')

    p = subprocess.Popen(cmd,
            stdin = subprocess.PIPE,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE)

    time.sleep(10)

   

    # Sleep for 1 second
    time.sleep(10)
    print("here")
    p.stdout.flush()
    os.write(p.stdin.fileno(), "change ide1-cd0 sample.iso\n\n".encode())


    time.sleep(5)
    print("here")

    os.write(p.stdin.fileno(), "sendkey esc\n".encode())
    time.sleep(3)
    #print("here")

    copy_cmd = " copy D:\\sample C:\\Users\\IEUser\\Desktop\\sample.exe\n"
    guest_type(copy_cmd, p)
    print("here")
    # Sleep for 5 seconds to make sure the guest finished it's tasks. 
    time.sleep(5)

    start_cmd = "start C:\\Users\\IEUser\\Desktop\\sample.exe"
    guest_type(start_cmd, p)
    os.write(p.stdin.fileno(), "begin_record sample\n".encode("utf-8"))

    # Now send the final \n that will launch the execution command.
    guest_type("\n", p)     

    time.sleep(5)

    guest_type("\n", p) 

    log_info("Started recording and executed the sample in the guest machine")


    log_info("Recording for: %d seconds"%(TIME_TO_EXECUTE))
    time.sleep(TIME_TO_EXECUTE)

    os.write(p.stdin.fileno(), "end_record\n".encode("utf-8"))
    os.write(p.stdin.fileno(), "q\n".encode("utf-8"))

    log_info("Recording is over, shutting the VM down")
    os.write(p.stdin.fileno(), "q\n".encode("utf-8"))
    time.sleep(3)

    while True:
        poll = p.poll()
        if poll == None:
            time.sleep(1)
        else:
            log_info("VM is shut down")
            break

    log_info("Finished recording the sample execution")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
            "-sample", 
            help = "The sample to executed",
            required = True)

    parser.add_argument(
            "-time",
            help = "The number of seconds to record an execution",
            type = int,
            default = 45)

    args = parser.parse_args(args = sys.argv[1:])

    record_execution(args.sample, args.time)
