
import pyshark

from subprocess import Popen, PIPE

def call_external_script(file_name, args):
    args =[file_name]+args
    process = Popen(args, stdout=PIPE, stderr=PIPE)

    stdout, stderr = process.communicate()
    return stdout

def parse_pcap(pcap_file, out_file):
    stdout = call_external_script("tshark", ["-r", pcap_file])
    cap = stdout.decode("utf-8")
    lines = cap.split("\n")


    #with open(out_file, "w") as f:
    #    for pkt in cap:
    #       f.write(str(pkt)+"\n")

    requests = []
    dump = False
    for line in lines:
        if len(line) == 0:
            continue
        chunks = [x for x in line.lstrip().split(" ") if x != '']
        #print(chunks)
        to = chunks[2]
        end = chunks[4]
        data = ' '.join(chunks[5:])
        requests.append({"to": to, "from": end, "data": data})
        if "PKIX-CRL 1013 Certificate Revocation List" in data:
            dump = True
    
    if dump:
        requests = [{"to": "n/a", "from": "n/a", "data": "DNS Lookup Error: Certificate Revocation: certificate revoked by the issuing CA and should not be trusted"}]
    
    return requests

        