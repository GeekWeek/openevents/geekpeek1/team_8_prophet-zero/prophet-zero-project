import os
import replay
import shutil

samples = os.listdir("sample_files")
for sample in samples:
    if sample == "2":
        continue

    shutil.copy(os.path.join("sample_files", sample,"sample-rr-nondet.log"), "sample-rr-nondet.log")
    shutil.copy(os.path.join("sample_files", sample, "sample-rr-snp"), "sample-rr-snp")

    replay.replay_recording("sample")

    shutil.copy("analysis.pcap", os.path.join("sample_files", sample, "analysis.pcap"))

    shutil.copy("countsc_syscalls.dat.gz", os.path.join("sample_files", sample, "countsc_syscalls.dat.gz"))

    shutil.copy("coverage.csv", os.path.join("sample_files", sample, "coverage.csv"))
    shutil.copy("foo.plog", os.path.join("sample_files", sample, "foo.plog"))
    shutil.copy("memsavep.raw", os.path.join("sample_files", sample, "memsavep.raw"))
    shutil.copy("replay_panda.stderr", os.path.join("sample_files", sample, "replay_panda.stderr"))
    shutil.copy("replay_panda.stdout", os.path.join("sample_files", sample, "replay_panda.stdout"))

    replay_movies = os.listdir()
    for rm in replay_movies:
        if "replay_movie" in rm:
            shutil.move(rm, os.path.join("sample_files", sample, rm))
