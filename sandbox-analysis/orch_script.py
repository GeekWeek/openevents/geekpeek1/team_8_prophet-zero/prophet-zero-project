import os 
import sandbox
import shutil
zoo_samples = os.listdir("zoo_samples")
id = 0
for sample in zoo_samples:
    out_dir = os.path.join("sample_files", str(id))
    if os.path.exists(out_dir):
        print("Folder out already exists")
    else:
        os.mkdir(out_dir)
        print("Successfully created folder out...")
    f = open(os.path.join("sample_files", str(id), str("id")+".txt"), "w")
    f.write(sample+"\n")

    sandbox.record_execution(os.path.join("zoo_samples", sample), 60)
    shutil.copy("sample-rr-nondet.log", os.path.join("sample_files", str(id), "sample-rr-nondet.log"))

    shutil.copy("sample-rr-snp", os.path.join("sample_files", str(id), "sample-rr-snp"))
    id+=1


