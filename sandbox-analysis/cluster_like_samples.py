# Features:
#  image dos headers - categorical
#  libraries, calls made (?) -binary
#  system calls - # of each seen
#  threads (continuous)
#  network traffic (binary)

from sklearn.cluster import KMeans
import numpy as np

def cluster_samples(data):
    kmeans = KMeans(n_clusters=16)
    labels = kmeans.fit_predict(data)
    return labels