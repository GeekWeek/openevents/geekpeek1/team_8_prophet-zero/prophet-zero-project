/* PANDABEGINCOMMENT
 *
 * Authors:
 *  Tim Leek               tleek@ll.mit.edu
 *  Ryan Whelan            rwhelan@ll.mit.edu
 *  Joshua Hodosh          josh.hodosh@ll.mit.edu
 *  Michael Zhivich        mzhivich@ll.mit.edu
 *  Brendan Dolan-Gavitt   brendandg@gatech.edu
 *
 * This work is licensed under the terms of the GNU GPL, version 2.
 * See the COPYING file in the top-level directory.
 *
PANDAENDCOMMENT */
// This needs to be defined before anything is included in order to get
// the PRIx64 macro
#define __STDC_FORMAT_MACROS

#include <zlib.h>

#include <capstone/capstone.h>
#if defined(TARGET_I386)
#include <capstone/x86.h>
#elif defined(TARGET_ARM)
#include <capstone/arm.h>
#elif defined(TARGET_PPC)
#include <capstone/ppc.h>
#elif defined(TARGET_MIPS)
#include <capstone/mips.h>
#endif

#include "panda/plugin.h"
#include "panda/common.h"
#include "panda/plog.h"

#include "argnums.h"

bool init_plugin(void *);
void uninit_plugin(void *);
bool translate_callback(CPUState *env, target_ulong pc);
int exec_callback(CPUState *env, target_ulong pc);

gzFile sclog;

// Check if the instruction is sysenter (0F 34)
bool translate_callback(CPUState *env, target_ulong pc) {
#if defined(TARGET_I386)
    unsigned char buf[2] = {};
    panda_virtual_memory_rw(env, pc, buf, 2, 0);
    // Check if the instruction is syscall (0F 05)
    if (buf[0]== 0x0F && buf[1] == 0x05) {
        return true;
    }
    // Check if the instruction is sysenter (0F 34)
    else if (buf[0]== 0x0F && buf[1] == 0x34) {
        return true;
    }
#endif
    return false;
}

#pragma pack(1)
typedef struct _syscall_entry {
    uint32_t asid;
    uint16_t ordinal;
    uint16_t num_args;
    uint64_t args[];
} syscall_entry;
#pragma pack()
            
int exec_callback(CPUState *env, target_ulong pc) {
#ifdef TARGET_I386
    CPUArchState *aenv = (CPUArchState *)env->env_ptr;

    uint32_t syscall = aenv->regs[R_EAX]; 
    int table = (syscall >> 12) & 0xf;
    int ordinal = (syscall & 0xfff);
    int num_args = 0;
    if (aenv->hflags & HF_LMA_MASK) { // 64-bit
        if (table == 0) { // nt
            assert(ordinal < (sizeof(win7_64_nt_args)/sizeof(win7_64_nt_args[0])));
            num_args = win7_64_nt_args[ordinal];
        }   
        else if (table == 1) { // win32k
            assert(ordinal < (sizeof(win7_64_win32k_args)/sizeof(win7_64_win32k_args[0])));
            num_args = win7_64_win32k_args[ordinal];
        }   
    }   
    else { // 32-bit
        if (table == 0) { // nt
            assert(ordinal < (sizeof(win7_32_nt_args)/sizeof(win7_32_nt_args[0])));
            num_args = win7_32_nt_args[ordinal];
        }   
        else if (table == 1) { // win32k
            assert(ordinal < (sizeof(win7_32_win32k_args)/sizeof(win7_32_win32k_args[0])));
            num_args = win7_32_win32k_args[ordinal];
        }   
    }   
    
    size_t sc_size = sizeof(syscall_entry) + sizeof(uint64_t)*num_args;
    syscall_entry *sc = g_malloc0(sc_size);
    sc->ordinal = syscall;
    sc->num_args = num_args;
    sc->asid = panda_current_asid(env);

    if (aenv->hflags & HF_LMA_MASK) { // 64-bit
        for (int i = 0; i < num_args; i++) {
            // Some in registers, some on the stack
            if (i == 0) sc->args[i] = aenv->regs[R_ECX];
            else if (i == 1) sc->args[i] = aenv->regs[R_EDX];
            else if (i == 2) sc->args[i] = aenv->regs[7];
            else if (i == 3) sc->args[i] = aenv->regs[7];
            else {
                // 64 bit windows stack arguments:
                // 8 byte return address
                // 0x20 byte spill area
                // first stack arg
                // second stack arg
                // ...
                uint64_t arg = 0;
                panda_virtual_memory_rw(env, aenv->regs[R_ESP] + 0x28 + (8*(i-4)),
                                        (uint8_t *) &arg, 8, false);
                sc->args[i] = arg;
            }
        }
    }
    else { // 32-bit
        // Easy -- all on the stack
        for (int i = 0; i < num_args; i++) {
            uint32_t arg = 0;
            // Windows 7 args are at EDX + 8
            panda_virtual_memory_rw(env, aenv->regs[R_EDX] + 8 + (4*i),
                                    (uint8_t *) &arg, 4, false);
            sc->args[i] = arg;
        }
    }   
            
    gzwrite(sclog, sc, sc_size);
#endif      
    return 0;                       
}           
        
bool init_plugin(void *self) {
    panda_cb pcb;
    
    panda_arg_list *args = panda_get_args("countsc");
    
    const char *prefix = panda_parse_string(args, "name", "countsc");
    char logfile[260] = {};
    sprintf(logfile, "%s_syscalls.dat.gz", prefix);
    sclog = gzopen(logfile, "w");
    if (!sclog) {
        perror("gzopen");
        return false;
    }
 
    pcb.insn_translate = translate_callback;
    panda_register_callback(self, PANDA_CB_INSN_TRANSLATE, pcb);
    pcb.insn_exec = exec_callback;
    panda_register_callback(self, PANDA_CB_INSN_EXEC, pcb);

    return true;
}

void uninit_plugin(void *self) {
    gzclose(sclog);
}