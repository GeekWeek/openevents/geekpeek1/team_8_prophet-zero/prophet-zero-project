from subprocess import Popen, PIPE

def call_external_script(file_name, args):
    args =[file_name]+args
    process = Popen(args, stdout=PIPE, stderr=PIPE)

    stdout, stderr = process.communicate()
    return stdout

import json
import re
def parse_data(plog_file, outfile, stdout_file):
    filename = "python3"
    args = ["/home/crazyeights/panda/panda/scripts/plog_reader.py", plog_file]
    output = call_external_script(filename, args)
    output = output.decode("utf-8")
    #print(output)
    out_plog = json.loads(output)
    
    #stdout_file = "replay_panda.stdout"
    target = "sample.exe"

    target_json = {}
    target_pids = []
    target_asids = []
    target_ppid = 0
    for i in out_plog:
        if "asidInfo" in i:
            pt = i["asidInfo"]
            #print(pt["names"][0])
            #if str(pt["pid"]) in target_pids:
            #    print(pt)
            
            if pt["names"][0] == target:
                target_json = pt
                target_pids.append(str(pt["pid"]))
                if "tids" in pt and len(pt["tids"])>0:
                    target_pids = target_pids + [str(t) for t in pt["tids"]]
                target_ppid = pt["ppid"]
                target_asids.append(str(hex(int(pt["asid"]))))
    
    
    target_pids = list(set(target_pids))

    parent_proc = ""
    child_procs = []
    child_pids = []
    for i in out_plog:
        if "asidInfo" in i:
            pt = i["asidInfo"]
            if str(pt["pid"]) == str(target_ppid):
                parent_proc = pt["names"][0]
                target_json["parent"]=parent_proc
        
            elif str(pt["ppid"]) in target_pids:
                child_procs.append(str(pt["pid"])+"-"+pt["names"][0])
                target_asids.append(str(hex(int(pt["asid"]))))
                child_pids.append(pt)

    target_asids = list(set(target_asids))
    f = open(stdout_file, "r")
    lines = f.read().split("\n")
    child_procs = list(set(child_procs))

    keyword = "Current process:"
    keyword_pattern = r'Current process: \S+ PID:(\d+) PPID:(\d+)'
    capture = False
    dynamic_lib_capture = False
    kernel_mod_capture = False
    current_pid = 0

    off_mod_capt = False
    off_lib_capt = False
    first_sighting = False

    libs = {}
    kernel_mods = {}
    libs_in_memory = []
    mods_in_memory = []
    dynamic_lib = "Dynamic libraries list"
    kernel_mod_list = "Kernel module list"
    
    for line in lines:
        if not capture:
            if keyword in line:
                m = re.match(keyword_pattern, line)
                if m:
                    pid = m[1]
                    ppid = m[2]
                    #print(pid)
                    if str(pid) in target_pids or str(pid) in child_pids:
                        capture = True
                        current_pid = pid
                        first_sighting = True
            elif dynamic_lib in line and not first_sighting:
                off_lib_capt = True
            elif kernel_mod_list in line and not first_sighting:
                off_mod_capt = True
            elif off_lib_capt and line == "":
                off_lib_capt = False
            elif off_mod_capt and line == "":
                off_mod_capt = False
            elif off_mod_capt:
                mod_name = line.split(" ")[-1]
                addr = line.lstrip().split(" ")[0]
                mods_in_memory.append(addr+"-"+mod_name)
            
            elif off_lib_capt:
                lib_name = line.split(" ")[-1]
                addr = line.lstrip().split(" ")[0]
                libs_in_memory.append(addr+"-"+lib_name)
        
        else:
            if dynamic_lib in line:
                dynamic_lib_capture = True
            
            elif dynamic_lib_capture and line == "":
                dynamic_lib_capture = False
            
            elif dynamic_lib_capture:
                addr = line.lstrip().split(" ")[0]
                lib_name = line.split(" ")[-1]
                if not current_pid in libs:
                    libs[current_pid]=[]
                if not addr+"-"+lib_name in libs_in_memory:
                    libs[current_pid].append(lib_name)
            
            if kernel_mod_list in line:
                kernel_mod_capture = True
            
            elif kernel_mod_capture and line == "":
                kernel_mod_capture = False
                capture=False
            
            elif kernel_mod_capture:
                mod_name = line.split(" ")[-1]
                addr = line.lstrip().split(" ")[0]
                if not current_pid in kernel_mods:
                    kernel_mods[current_pid]=[]
                if not addr+"-"+mod_name in mods_in_memory: 
                    kernel_mods[current_pid].append(mod_name)
        
    
    for k, v in kernel_mods.items():
        kernel_mods[k]=list(set(v))
    
    for k, v in libs.items():
        libs[k]=list(set(v))

    """"
    target_file = outfile
    with open(target_file, "w") as f:
        f.write(str(target_json)+"\n")
        f.write(str(child_procs)+"\n")
        f.write(str(libs)+"\n")
        f.write(str(kernel_mods)+"\n")
    """
    #print(target_asids)
    if not "asid" in target_json:
        return None, None, [], [], [], [], []
    return str(hex(int(target_json["asid"]))), target_json, child_procs, libs, kernel_mods, target_pids, target_asids

