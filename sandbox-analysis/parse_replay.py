import re
import json
def parse_replay(in_file, out_file, asid=[]):
    f  =  open(in_file, "r")
    lines = f.read().split("\n")
    
    p = r'(a-f0-9)+:\s(\w+)\(FileHandle=[0-9a-f]+,\sEvent=\d+,\sApcRoutine=\d+,\sApcContext=[a-f0-9]+,\sIoStatusBlock=[a-f0-9]+,\sBuffer=\d+,\sBufferLength=([0-9a-f]+),\sByteOffset=[0-9a-f]+,\sKey=\d+\)'

    calls = []
    for line in lines:
        if "(error)" in line:
            parts = line.split(" ")
            casid = "0x"+parts[0][:-1]
            call_name = parts[2]
            rc = parts[3][:-1]
            filename =  parts[5].split("=")[1]
            if len(asid) == 0 or casid in asid:
                #calls.append({"call": call_name, "return": rc, "filename": filename})
                calls.append({"call": call_name, "return": rc, "filename": filename})
        
        elif "Returning from" in line:
            parts = line.split(" ")
            casid = "0x"+parts[0][:-1]
            call_name = parts[3]
            rc = parts[4][:-1]
            filename =  parts[6].split("=")[1]
            if len(asid) == 0 or casid in asid:
                #calls.append({"call": call_name, "return": rc, "filename": filename})
                calls.append({"call": call_name, "filename": filename})


        elif "(" in line:
            m = re.match(p, line)
            if m:
                casid = "0x"+m[1]
                call_name = m[2]
                buffer_len = m[3]
                if len(asid) == 0 or casid in asid:
                    calls.append({"call": call_name, "buffer_len": buffer_len})
            
    rstr = json.dumps(calls, indent=2)
    with open(out_file, "w") as f:
        f.write(rstr)
    return calls
