import parse_dat as pdat
import parse_pcap as ppcap
import parse_plog as pplog
import parse_replay as preplay
import json
import os

import static_analysis as static_anal

def analyse(dir_name, out_name, file_name):
    
    dat_file = os.path.join(dir_name, "countsc_syscalls.dat.gz")
    pcap_file = os.path.join(dir_name, "analysis.pcap")
    plog_file = os.path.join(dir_name, "foo.plog")

    pstdout_file = os.path.join(dir_name, "replay_panda.stdout")
    pstderr_file = os.path.join(dir_name, "replay_panda.stderr")

    sc_data = out_name+"_syscalls.txt"
    net_data = out_name+"_net.txt"
    file_data = out_name+"_fileio.txt"
    proc_data = out_name+"_proc.txt"

    asid, target_json, child_procs, libs, kernel_mods, tids, tasids = pplog.parse_data(plog_file, proc_data, pstdout_file)
    if asid == None:
        raise Exception("No asid in data. file: {0} didn't run".format(file_name))
    print(asid)
    syscalls = pdat.parse_sc_dat(dat_file, sc_data, tasids)
    networks = ppcap.parse_pcap(pcap_file, net_data)
    fileio = preplay.parse_replay(pstderr_file, file_data, tasids)

    static_data, pe_header, libs_list = static_anal.get_file_info(file_name)
    
    #static_out_file = out_name+"_static_data.json"
    #with open(os.path.join("out", static_out_file), "w") as f:
    #    json.dump(static_data, f, indent=4)

    out_file = out_name+"_data.json"
    data = {
        "static": 
            static_data,
        "dynamic": {
        "name": file_name,
        "class": "WIP",
        "parent": target_json["parent"],
        "pid":  target_json["pid"],
        "no_threads": len(tids)-1,
        "children": child_procs,
        "kernel_mods": kernel_mods,
        "libraries": libs,
        "syscalls_list": syscalls,
        "network": networks,
        "file_io": fileio
        }
    }

    with open(os.path.join("out", out_file), "w") as f:
        json.dump(data, f, indent=4)
    
    return pe_header, libs_list, syscalls, len(tids)-1, int(len(networks) > 0)
    

#analyse("sample2_h", "hupigon")

