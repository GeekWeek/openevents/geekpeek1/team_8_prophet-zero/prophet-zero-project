job "prophet-zero" {
  datacenters = ["scs"]

  group "client" {
    network {
      port "frontend" {
        to = 80
      }
    }

    service {
      name = "prophet-zero-quasar-server"
      port = "frontend"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.prophet-zero-quasar-server.rule=Host(`prophet-zero.h4tt.ca`)",
        "traefik.http.routers.prophet-zero-quasar-server.entrypoints=https",
        "traefik.http.routers.prophet-zero-quasar-server.tls.certresolver=letsencrypt",
      ]

      check {
        type     = "http"
        port     = "frontend"
        path     = "/"
        interval = "5s"
        timeout  = "2s"

        check_restart {
          limit           = 3
          grace           = "30s"
          ignore_warnings = false
        }
      }
    }

    task "quasar" {
      driver = "docker"

      config {
        image = "registry.gitlab.com/geekweek/openevents/geekpeek1/team_8_prophet-zero/prophet-zero-project/prophet-zero-quasar-server:latest"
        ports = ["frontend"]
      }

      resources {
        cpu    = 1000
        memory = 1024
      }
    }
  }

  group "backend" {
    network {
      mode = "bridge"

      port "api" {
        to = 8000
      }
    }

    service {
      name = "prophet-zero-rocket-server"
      port = "api"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.prophet-zero-rocket-server.rule=Host(`api.prophet-zero.h4tt.ca`)",
        "traefik.http.routers.prophet-zero-rocket-server.entrypoints=https",
        "traefik.http.routers.prophet-zero-rocket-server.tls.certresolver=letsencrypt",
      ]

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "prophet-zero-mongo-database"
              local_bind_port  = 27017
            }
          }
        }
      }
    }

    task "rocket" {
      driver = "docker"

      config {
        image = "registry.gitlab.com/geekweek/openevents/geekpeek1/team_8_prophet-zero/prophet-zero-project/prophet-zero-rocket-server:latest"
        ports = ["api"]
      }

      resources {
        cpu    = 4096
        memory = 1024
      }
    }
  }

  group "database" {
    network {
      mode = "bridge"
    }

    service {
      name = "prophet-zero-mongo-database"
      port = "27017"

      connect {
        sidecar_service {}
      }
    }

    task "mongo" {
      driver = "docker"

      config {
        image = "mongo"
        ports = ["mongo"]
      }

      resources {
        cpu    = 1000
        memory = 1024
      }
    }
  }
}
