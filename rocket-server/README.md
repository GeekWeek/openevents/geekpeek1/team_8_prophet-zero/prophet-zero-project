# P.R.O.P.H.E.T. Rocket Backend

This server is written in rocket.rs and exposes a REST API to interact with P.R.O.P.H.E.T.'s database.

## Codegen

To run the codegen workflow, run `make codegen`. This assumes that the database migrations have already been applied.
