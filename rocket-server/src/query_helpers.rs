//! Helper types for query params.

use std::ops::Deref;

use async_trait::async_trait;
use chrono::NaiveDate;
use rocket::form::{self, FromFormField, ValueField};

/// A date query parameter, which should be formatted as "%Y-m-d".
#[derive(Debug)]
pub struct QueryDate(NaiveDate);

#[async_trait]
impl<'r> FromFormField<'r> for QueryDate {
    fn from_value(field: ValueField<'r>) -> form::Result<'r, Self> {
        let s = field.value;
        let date = NaiveDate::parse_from_str(s, "%Y-%m-%d").expect("Failed to parse date?");
        //.map_err(|e| form::Error::validation(format!("Failed to parse date: {}", e)))?;
        Ok(QueryDate(date))
    }
}

impl Deref for QueryDate {
    type Target = NaiveDate;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
