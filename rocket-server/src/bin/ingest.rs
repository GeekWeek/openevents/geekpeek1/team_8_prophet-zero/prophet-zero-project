use std::{fs::File, path::PathBuf};

use anyhow::Result;
use mongodb::{options::ClientOptions, Client, Collection};
use rocket::tokio;
use rocket_server::{
    database::{COLLECTION_NAME, DB_NAME},
    schema::{MalwareData, MalwareDocument},
};
use structopt::StructOpt;

/// A simple program to ingest JSON data and insert it into PROPHET's database.
#[derive(StructOpt)]
struct Cli {
    /// Directory where JSON files are located.
    json_path: PathBuf,
}

#[tokio::main]
async fn main() -> Result<()> {
    let cli = Cli::from_args();
    println!("Connecting to DB");
    let options = ClientOptions::parse("mongodb://localhost:27017").await?;
    let client = Client::with_options(options)?;

    drop_collection(&client).await?;

    let dir_len = std::fs::read_dir(cli.json_path.clone())?
        .filter_map(Result::ok)
        .count();

    for (i, dent) in std::fs::read_dir(cli.json_path)?
        .filter_map(Result::ok)
        .enumerate()
    {
        println!("({}/{}) Processing {}", i, dir_len, dent.path().display());

        let path = dent.path();
        let file = File::open(path.clone()).expect("Couldn't open file");
        let data: MalwareData = match serde_json::from_reader(file) {
            Ok(data) => data,
            Err(e) => {
                eprintln!("Warning: failed to parse {}: {}", dent.path().display(), e);
                continue;
            }
        };
        let file_name = path
            .file_name()
            .ok_or(anyhow::anyhow!("Failed to get filename"))?
            .to_string_lossy();
        let base_name = file_name.strip_suffix("_data.json").unwrap_or(&file_name);
        let name = base_name.replace("_exe", ".exe");
        let document = MalwareDocument::new(name, data);
        insert_document(&client, document).await?;
    }
    Ok(())
}

async fn drop_collection(client: &Client) -> Result<()> {
    let database = client.database(DB_NAME);
    let collection: Collection<MalwareDocument> = database.collection(COLLECTION_NAME);
    collection.drop(None).await?;
    Ok(())
}

async fn insert_document(client: &Client, doc: MalwareDocument) -> Result<()> {
    let database = client.database(DB_NAME);
    let collection = database.collection(COLLECTION_NAME);
    collection.insert_one(doc, None).await?;
    Ok(())
}
