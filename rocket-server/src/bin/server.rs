use rocket::{catch, catchers, launch, routes, Request};
use rocket_db_pools::Database;
use rocket_server::{cors::CORS, database::Db, resources::malware};

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("No such endpoint: {}", req.uri())
}

#[catch(default)]
fn default_catcher(req: &Request) -> String {
    format!("Error while handling request: {}", req.uri())
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Db::init())
        .attach(CORS)
        .register("/", catchers![not_found, default_catcher])
        .mount("/", routes![malware::all_basic, malware::by_name,])
}
