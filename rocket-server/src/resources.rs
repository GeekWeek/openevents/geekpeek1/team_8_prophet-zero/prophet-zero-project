//! This module defines URI endpoints for each resource in the [`crate::schema`].

use crate::{
    database::{Db, COLLECTION_NAME, DB_NAME},
    query_helpers::QueryDate,
    schema::MalwareDocument,
};
use futures::TryStreamExt;
use mongodb::{
    bson::{doc, Document, Regex},
    options::FindOptions,
};
use rocket::{get, response::status::NotFound, serde::json::Json};
use rocket_db_pools::Connection;
use serde::Serialize;

const DEFAULT_PER_PAGE: usize = 64;

pub mod malware {

    use super::*;

    #[derive(Debug, Serialize)]
    pub struct MalwarePage {
        page: usize,
        per_page: usize,
        num_pages: usize,
        data: Vec<MalwareDocument>,
    }

    /// Get basic info about all malware with optional filtering.
    #[get(
       "/malware?<page>&<per_page>&<name>&<arch>&<language>&<size_gte>&<size_lte>&<date_before>&<date_after>&<libraries>&<files_touched>"
   )]
    pub async fn all_basic(
        conn: Connection<Db>,
        name: Option<&str>,
        arch: Option<&str>,
        language: Option<&str>,
        size_gte: Option<i64>,
        size_lte: Option<i64>,
        libraries: Option<&str>,
        files_touched: Option<&str>,
        date_before: Option<QueryDate>,
        date_after: Option<QueryDate>,
        page: Option<usize>,
        per_page: Option<usize>,
    ) -> Result<Json<MalwarePage>, NotFound<String>> {
        let db = conn
            .into_inner()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let mut filter = Document::new();
        // Filter by name
        if let Some(name) = name {
            filter.insert(
                "_id",
                doc! {
                    "$regex": name,
                },
            );
        }
        // Filter by arch
        if let Some(arch) = arch {
            filter.insert(
                "static.arch",
                doc! {
                    "$regex": arch,
                },
            );
        }
        // Filter by language
        if let Some(language) = language {
            filter.insert(
                "static.lang",
                doc! {
                    "$regex": language,
                },
            );
        }
        // Filter by libraries touched
        if let Some(libraries) = libraries {
            let mut library_filter = Vec::new();
            for lib in libraries.split_terminator(',') {
                library_filter.push(doc! {
                    "static.libraries": doc! {
                        "$elemMatch": doc! {
                            "lib_name": doc! {
                                "$regex": &lib
                            }
                        }
                    }
                });
            }
            if library_filter.len() > 0 {
                filter.insert("$or", library_filter);
            }
        }
        // Filter by files touched
        if let Some(files) = files_touched {
            let mut file_filter = Vec::new();
            for file in files.split_terminator(',') {
                file_filter.push(doc! {
                    "static.files": doc! {
                        "$elemMatch": doc! {
                            "$regex": &file
                        }
                    }
                });
            }
            if file_filter.len() > 0 {
                filter.insert("$or", file_filter);
            }
        }
        // Filter by size >=
        if let Some(size_gte) = size_gte {
            filter.insert(
                "static.filesize",
                doc! {
                    "$gte": &size_gte
                },
            );
        }
        // Filter by size <=
        if let Some(size_lte) = size_lte {
            filter.insert(
                "static.filesize",
                doc! {
                    "$lte": &size_lte
                },
            );
        }
        // // Filter by before date
        // if let Some(date_before) = date_before {
        //     let date_time = *date_before;
        //     conditions = conditions.add(schema::Column::CompilationDate.lte(date_time));
        // }
        // // Filter by after date
        // if let Some(date_after) = date_after {
        //     let date_time = *date_after;
        //     conditions = conditions.add(schema::Column::CompilationDate.gte(date_time));
        // }
        println!("{}", filter);
        let page = page.unwrap_or(0);
        let per_page = per_page.unwrap_or(DEFAULT_PER_PAGE);
        let count = db
            .count_documents(filter.clone(), None)
            .await
            .map_err(|e| NotFound(e.to_string()))?;
        let num_pages = div_up(count as usize, per_page);
        let mut options = pagination_options(per_page as i64, page as u64);
        options.projection = Some(doc! {
            "_id": 1i32,
            "static.arch": 1i32,
            // "static.lang": 1i32,
            // "static.class": 1i32,
            // "static.bintype": 1i32,
            // "static.hash": 1i32,
            // "static.os": 1i32,
            // "static.filesize": 1i32,
        });
        let data: Vec<_> = db
            .find(filter, options)
            .await
            .map_err(|e| NotFound(e.to_string()))?
            .try_collect()
            .await
            .map_err(|e| NotFound(e.to_string()))?;
        let page = MalwarePage {
            page,
            per_page,
            num_pages,
            data,
        };
        Ok(Json(page))
    }

    /// Get specific malware data by name. This returns the entire malware object.
    #[get("/malware/<name>")]
    pub async fn by_name(
        conn: Connection<Db>,
        name: String,
    ) -> Result<Json<MalwareDocument>, NotFound<String>> {
        let db = conn
            .into_inner()
            .database(DB_NAME)
            .collection(COLLECTION_NAME);
        let data: MalwareDocument = db
            .find_one(
                doc! {
                    "_id": &name,
                },
                None,
            )
            .await
            .map_err(|e| NotFound(e.to_string()))?
            .ok_or_else(|| NotFound(format!("No such malware {}", name)))?;
        Ok(Json(data))
    }
}

fn pagination_options(per_page: i64, page: u64) -> FindOptions {
    let skip = page * per_page as u64;
    FindOptions::builder()
        .limit(Some(per_page))
        .skip(Some(skip))
        .sort(doc! { "_id": 1i32 })
        .build()
}

pub fn div_up(a: usize, b: usize) -> usize {
    (a + (b - 1)) / b
}

// let page = page.unwrap_or(0);
// let per_page = per_page.unwrap_or(DEFAULT_PER_PAGE);
// let mut conditions = Condition::all();
// // Execute the paginated query
// let paginator = schema::Entity::find()
//     .filter(conditions)
//     .order_by_asc(schema::Column::Id)
//     .paginate(db, per_page);
// let num_pages = paginator
//     .num_pages()
//     .await
//     .expect("Failed to get number of pages?");
// let data = paginator
//     .fetch_page(page)
//     .await
//     .map_err(|e| NotFound(e.to_string()))?;
// let malware = MalwarePage {
//     data,
//     page,
//     num_pages,
//     per_page,
// };
