use anyhow::Result;
use async_trait::async_trait;
use mongodb::{options::ClientOptions, Client};
use rocket::figment::Figment;
use rocket_db_pools::{Config, Database, Error as PoolsError, Pool};
use std::{ops::Deref, time::Duration};

pub const DB_NAME: &str = "prophet";
pub const COLLECTION_NAME: &str = "malware";

pub struct ClientUnit(Client);

impl Deref for ClientUnit {
    type Target = Client;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Database)]
#[database("mongodb")]
pub struct Db(ClientUnit);

#[async_trait]
impl Pool for ClientUnit {
    type Error = PoolsError<mongodb::error::Error, std::convert::Infallible>;

    type Connection = Client;

    async fn init(figment: &Figment) -> Result<Self, Self::Error> {
        let config = figment.extract::<Config>()?;
        let mut opts = ClientOptions::parse(&config.url)
            .await
            .map_err(PoolsError::Init)?;
        opts.min_pool_size = config.min_connections;
        opts.max_pool_size = Some(config.max_connections as u32);
        opts.max_idle_time = config.idle_timeout.map(Duration::from_secs);
        opts.connect_timeout = Some(Duration::from_secs(config.connect_timeout));
        Ok(ClientUnit(
            Client::with_options(opts).map_err(PoolsError::Init)?,
        ))
    }

    async fn get(&self) -> Result<Self::Connection, Self::Error> {
        Ok(self.0.clone())
    }
}
