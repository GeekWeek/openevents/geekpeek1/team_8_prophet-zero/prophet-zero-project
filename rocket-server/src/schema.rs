use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, net::IpAddr};

#[derive(Debug, Serialize, Deserialize)]
pub struct MalwareDocument {
    _id: String,
    #[serde(flatten)]
    data: MalwareData,
}

impl MalwareDocument {
    pub fn new(name: String, data: MalwareData) -> Self {
        MalwareDocument { _id: name, data }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MalwareData {
    #[serde(rename = "static")]
    _static: Option<StaticData>,
    dynamic: Option<DynamicData>,
    ml: Option<MlData>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StaticData {
    #[serde(default)]
    arch: String,
    bintype: Option<String>,
    class: Option<String>,
    hash: Option<String>,
    filesize: Option<i64>,
    #[serde(skip)] // FIXME: skipping for now
    #[allow(dead_code)]
    compiled: Option<NaiveDateTime>,
    os: Option<String>,
    lang: Option<String>,
    libraries: Option<Vec<StaticLibrary>>,
    strings: Option<Vec<StaticString>>,
    iocs: Option<Vec<StaticIoc>>,
    files: Option<Vec<String>>,
    cmds: Option<Vec<String>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StaticLibrary {
    name: Option<String>,
    lib_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StaticString {
    section: Option<String>,
    #[serde(rename = "type")]
    _type: Option<String>,
    string: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StaticIoc {
    indicator_type: Option<String>,
    value: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DynamicData {
    name: Option<String>,
    class: Option<String>,
    parent: Option<String>,
    pid: Option<i64>,
    no_threads: Option<i64>,
    children: Option<Vec<String>>,
    kernel_mods: Option<HashMap<String, Vec<String>>>,
    libraries: Option<HashMap<String, Vec<String>>>,
    syscalls_list: Option<Vec<String>>,
    network: Option<Vec<DynamicNetwork>>,
    file_io: Option<Vec<DynamicFileIo>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DynamicNetwork {
    to: Option<String>,
    from: Option<String>,
    data: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DynamicFileIo {
    call: Option<String>,
    filename: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MlData {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_smoke() {
        let data = r#"
{
    "static": {
        "arch": "x86",
        "bintype": "pe",
        "class": "PE32",
        "compiled": "Thu Sep 20 02:50:38 2012",
        "os": "windows",
        "lang": "c",
        "libraries": [
            {
                "name": "FreeLibrary",
                "lib_name": "KERNEL32.DLL"
            }
        ],
        "strings": [
            {
                "section": "",
                "type": "ascii",
                "string": "!This program cannot be run in DOS mode.\\r\\r\\n$"
            }
        ],
        "iocs": [
            {
                "indicator_type": "technique",
                "value": "net"
            }
        ],
        "files": [
            "SHELL32.dll",
            "Packet.dll",
            "\\\\Application Data\\\\Bitcoin\\\\wallet.dat"
        ],
        "cmds": [
            "start",
            "Path",
            "SET DST_ADDR "
        ]
    },
    "dynamic": {
        "name": "zoo_samples/dumped.exe",
        "class": "WIP",
        "parent": "cmd.exe",
        "pid": 2644,
        "no_threads": 6,
        "children": [],
        "kernel_mods": {
            "2644": []
        },
        "libraries": {
            "2644": [
                "C:\\Windows\\system32\\MSWSOCK.dll",
                "C:\\Windows\\system32\\user32.dll",
                "C:\\Users\\IEUser\\Desktop\\sample.exe"
            ]
        },
        "syscalls_list": [],
        "network": [
            {
                "to": "10.0.2.15",
                "from": "64.4.54.254",
                "data": "TLSv1 220 Client Key Exchange, Change Cipher Spec, Encrypted Handshake Message"
            }
        ],
        "file_io": [
            {
                "call": "NtOpenFile",
                "filename": "\\??\\C:\\Users\\IEUser\\,"
            }
        ]
    },
    "ml": {}
}
            "#;

        let _: MalwareData = serde_json::from_str(data).expect("should deserialize!");
    }
}
