export default {
  setPeople(state, people) {
    state.people = people;
  },

  addPersonToState(state, person) {
    state.people.push(person);
  }
}