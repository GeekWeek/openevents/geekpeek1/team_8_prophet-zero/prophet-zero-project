import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'

import people from './people'
import malware from './malware'


export default createStore({
  modules: {
    people,
    malware
  },

  strict: process.env.DEBUGGING
})